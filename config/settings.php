<?php
return array(
    /*
     * Is email activation required
     */
    'activation' => env('ACTIVATION', false)
);