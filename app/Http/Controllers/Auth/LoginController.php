<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\UserRole;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/notice-board';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the OAuth Provider.
     * 
     * @param String $provider 
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * 
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     * 
     * @param string $provider 
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        if(isset($user->email))
       	 	$authUser = $this->findOrCreateUser($user, $provider);
        else
        {
        	$socialEmail = $user->id.'@asset.co';
        	$authUser = $this->registerUserWithoutEmail($user, $provider,$socialEmail);
        }

        Auth::login($authUser, true);
        
        $fetchUser = Auth::user();

        if ($fetchUser->is_blocked == 1) {
            Auth::logout();
            return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors(['email' => 'Your account is inactive. Please contact the administrator for help.']);
        }elseif ($fetchUser->address_verified == 0) {
            return redirect()->route('confirm-address');
        }elseif ($fetchUser->address_verified == 1) {
            return redirect()->route('verification.type');
        }else{
            // Send the normal successful login response
            return redirect($this->redirectTo);
        }
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * 
     * @param \App\User $user     Socialite user object
     * @param string    $provider Social auth provider
     * 
     * @return User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('email', $user->email)->first();
        
        if ($authUser) {
            return $authUser;
        }
        
        //save user
        $registeredUser = new User;
        $registeredUser->name = $user->name;
        $registeredUser->email = $user->email;
        $registeredUser->phone_number = $user->phone ?? null;
        $registeredUser->provider = $provider;
        $registeredUser->provider_id = $user->id;
        $registeredUser->save();

        //save user role
		$role = new UserRole;
		$role->user_id = $registeredUser->id;
		$role->role_id = 2;
		$role->save();

        return $registeredUser;
    }


    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * 
     * @param \App\User $user     Socialite user object
     * @param string    $provider Social auth provider
     * @param string    $socialEmail Social auth email
     * 
     * @return User
     */
    public function registerUserWithoutEmail($user, $provider, $socialEmail)
    {
        $authUser = User::where('email', $socialEmail)->first();
        
        if ($authUser) {
            return $authUser;
        }
        
        //save user
        $registeredUser = new User;
        $registeredUser->name = $user->name;
        $registeredUser->email = $socialEmail;
        $registeredUser->phone_number = $user->phone ?? null;
        $registeredUser->provider = $provider;
        $registeredUser->provider_id = $user->id;
        $registeredUser->save();

        //save user role
		$role = new UserRole;
		$role->user_id = $registeredUser->id;
		$role->role_id = 2;
		$role->save();

        return $registeredUser;
    }

    /**
     * Custom login credentials
     *
     * @params array $request
     *
     * @return void
     */
    public function login(Request $request)
    {
        // Make sure the user is active and not deleted so logged in
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            if ($user->is_blocked == 1) {
                Auth::logout();
                return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors(['email' => 'Your account is inactive. Please contact the administrator for help.']);
            }elseif ($user->address_verified == 0) {
                return redirect()->route('confirm-address');
            }elseif ($user->address_verified == 1) {
                return redirect()->route('verification.type');
            }else{
                // Send the normal successful login response
                return $this->sendLoginResponse($request);
            }
        }
        return $this->sendFailedLoginResponse($request);
    }
}
