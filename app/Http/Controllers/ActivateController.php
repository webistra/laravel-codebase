<?php
/**
 * ActivateController File Document Comment
 *
 * PHP Version 7.1
 *
 * @category Controllers
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers
 */
namespace App\Http\Controllers;

use App\User;
use Exception;

/**
 * ActivateController Class Doc Comment
 *
 * @category Class
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers
 */

class ActivateController extends Controller
{
    /**
     * Activate email verification function
     *
     * @param  $token
     * @return \Illuminate\Http\Response
    */
    public function activate($token)
    {
        try{    
            if (auth()->user()->email_verified) {

                return redirect()->route('home')
                    ->with('status', 'success')
                    ->with('message', 'Your email is already activated.');
            }

            $activation = User::where('remember_token', $token)
                ->where('id', auth()->user()->id)
                ->first();

            if (empty($activation)) {

                return redirect()->route('home')
                    ->with('status', 'wrong')
                    ->with('message', 'No such token in the database!');

            }

            auth()->user()->email_verified = true;
            auth()->user()->save();

            //$activation->delete();
            $activation->where('remember_token',$token)->update(['remember_token'=>'']);

            session()->forget('above-navbar-message');

            return redirect()->route('home')
                ->with('status', 'success')
                ->with('message', 'You successfully activated your email!');
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }

    }

}
