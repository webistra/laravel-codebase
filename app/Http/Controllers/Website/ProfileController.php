<?php
/**
 * ProfileController File Document Comment
 *
 * PHP Version 7.1
 *
 * @category Controllers
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Website
 */
namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use DB;
use Validator;
use Carbon\Carbon;
use Auth;
use Hash;
use App\Rules\EmailExists;
use App\Rules\PhoneNumberExists;
use App\Http\Controllers\Helper\Helper;
use Toastr;
use App\PropertyFollow;

/**
 * ProfileController Class Doc Comment
 *
 * @category Class
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Website
 */

class ProfileController extends Controller
{
    /**
     * Function Name : loadProfile
     * Description   : load profile view
     *
     * @return response
    */
    public function loadProfile()
    {
    	$property_following_count = PropertyFollow::where('user_id',auth()->user()->id)->count();
        return view('website.profile',compact('property_following_count'));
    }


    /**
     * Function Name : saveEditProfile
     * Description   : save user's edit profile
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
    public function saveEditProfile(Request $request)
    {
        
        $validatedData = $request->validate([
            'email' => ['required','email',new EmailExists],
            'full_name' => 'required|min:2|max:90',
            'phone_number' => ['required','min:6','max:32',new PhoneNumberExists],
            'street_address' => 'required|max:300',
            'referral_code' => 'nullable|max:10',
            'file' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'apartment' => 'required'
        ]);

        try{    
                $user = User::find(auth()->user()->id);
                $user->email =  $request->email;
                $user->name =  $request->full_name;
                $user->phone_number = $request->phone_number;
                $user->street_address = $request->street_address;
                $user->date_of_birth = $request->date_of_birth;
                $user->apartment = $request->apartment;
                $user->gender = $request->gender;
                
                
                //to upload photo 
                if($request->hasFile('file')) {
                    $image = $request->file('file');
                    $image =  Storage::putFileAs('uploads/profile_images', $image, $image->getClientOriginalName());
                    $user->profile_image = $image;
                }

                $user->save();
                
                Toastr::success('Your profile details updated successfully', '', ["positionClass" => "toast-bottom-right"]);
            
                return redirect('profile');
        } catch (Exception $e) {
                return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : validatePassword
     * Description   : validate password
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
    public function validatePassword(Request $request)
    {   try{    
            $user_id = auth()->user()->id;
            $user = User::find($user_id);
            if (Hash::check($request->old_password, $user->password)) {
            return 1;
            }
            return 0;
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : updatePassword
     * Description   : update user's password
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
    public function updatePassword(Request $request)
    {
        try{    
            $user_id = auth()->user()->id;
            $password = $request->password;
            $user = User::find($user_id);
            $user->password = Hash::make($password);
            $user->update(); //or $user->save();
            
            Toastr::success('Your password changed successfully', '', ["positionClass" => "toast-bottom-right"]);

            return redirect('profile');
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }


    /**
     * Function Name : loadEditProfile
     * Description   : load edit profile page
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
     public function loadEditProfile(Request $request)
     {
        try{ 
            $user = auth()->user();
            return view('edit-profile',compact('user'));
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
     }

    /**
     * Function Name : confirmAddress
     * Description   : load confirm address html
     *
     * @return response
    */
    public function confirmAddress()
    {
        try{ 
            return view('website.confirm_address');
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : confirmAddressSubmit
     * Description   : validate user's confirm address
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
    public function confirmAddressSubmit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'zipCode' => 'required|min:5|max:10',
            'street_address' => 'required|max:200',
            'apartment' => 'required|max:200',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator->errors());
        }
        try
        { 
            $findUser = User::find(auth()->user()->id);
            $findUser->street_address = $request->street_address;
            $findUser->apartment = $request->apartment;
            $findUser->zip_code = $request->zipCode;
            $findUser->address_verified = 1;
            
            if($findUser->save()) {
                if(!empty($findUser->provider) && $findUser->address_verified == 1 )
                    return redirect()->route('mobile-number');
                else    
                    return redirect()->route('verification.type');
            }
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : verificationType
     * Description   : load verification type view
     *
     * @return response
    */
    public function verificationType() 
    { 
        try{
            return view('website.verification_type');
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : sendOTP
     * Description   : send otp to user's phone number
     *
     * @return response
    */
    public function sendOTP() 
    { 
        try{
            $otpRandom = rand(1000,9999);
            $message = 'Your OTP from AssetsCO is '.$otpRandom; 
            $updateOTP = User::where('id',auth()->user()->id)->update(['otp' => $otpRandom]);
            $getPhone = User::find(auth()->user()->id);
            $userPhoneNumber = $getPhone->country_code.$getPhone->phone_number;
            if($updateOTP) {
                $sendMsg = Helper::sendSms($userPhoneNumber, $message);
            }
            return true;
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }
    
    /**
     * Function Name : confirmAddressSubmit
     * Description   : submit user's verification type request
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
    public function verificationtypeSubmit(Request $request) 
    { 
        try{
                $documentUpload = $request->docVerify;
                $getUser = User::find(auth()->user()->id);
                 // Documents Check
                if (!empty($documentUpload)) {
                    $documentUpload =  Storage::putFileAs('uploads/document_upload', $documentUpload, $documentUpload->getClientOriginalName());
                    $getUser->documents = $documentUpload;
                    $getUser->update();

                    session()->flash('status','Your documents has been uploaded. Please give us time to review your documents.');
                    session()->flash('alert-class','alert-info');
                     return redirect()->back();
                }else { 
                    session()->flash('status','Please upload documents.');
                    session()->flash('alert-class','alert-info');
                    return redirect()->back();
                }
          
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : viewOtp
     * Description   : load otp verification page
     *
     * @return response
    */
    public function viewOtp() 
    { 
        return view('website.otp_verification');
    }

    /**
     * Function Name : verificationTypesubmission
     * Description   : submit user's verification type and save into database
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
    public function verificationTypesubmission(Request $request) 
    { 
        try{
                $otp = $request->pin;
                if(!empty($otp))
                {
                    $implodeOTP = implode('', $otp);
                    $getUserID = User::find(auth()->user()->id);
                    $storeOTP = $getUserID->otp;
                    $isVerified = $getUserID->address_verified;

                     if($storeOTP == $implodeOTP && $isVerified == 1) {
                        $getUserID->address_verified = 2;
                        $getUserID->otp = 0;
                        $getUserID->update();
                        session()->flash('message','OTP verified successfully');
                        session()->flash('alert-class','alert-info');
                        return redirect()->to('/notice-board');
                     }else{
                        session()->flash('message','Your OTP does not match.');
                        session()->flash('alert-class','alert-info');
                        return redirect()->back();
                     }
                }
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : verificationTypesubmission
     * Description   : Check verification type for otp or document.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
    public function checkVerificationType(Request $request) 
    { 
        try{
             if($request->verify == 'phone')
             {
                $this->sendOTP();
                return redirect()->route('otp-verification');
             }
             elseif($request->verify == 'document')
                return redirect()->route('document-verification');
             else
                return redirect()->to('/notice-board');
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : updateMobileNumber
     * Description   : Update mobile number via social login
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
    public function updateMobileNumber(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|min:6|max:20',new PhoneNumberExists,
            'country_code' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator->errors());
        }

        try{ 
                $findUser = User::find(auth()->user()->id);
                $findUser->country_code = $request->country_code;
                $findUser->phone_number = $request->phone_number;
                $findUser->save();
                
                $this->sendOTP();
                
                return redirect()->route('otp-verification');

        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : loadChangePasswordTemplate
     * Description   : Load change password template
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
    public function loadChangePasswordTemplate()
    {   
        return view('website.change_password');
    }
}    