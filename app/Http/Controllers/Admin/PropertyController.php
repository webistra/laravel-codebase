<?php
/**
 * PropertyController File Document Comment
 *
 * PHP Version 7.1
 *
 * @category Controllers
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Admin
 */
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Property;
use App\PropertyImage;

/**
 * ProfileController Class Doc Comment
 *
 * @category Class
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Admin
 */

class PropertyController extends Controller
{
   /**
     * Function Name : propertyList
     * Description   : load property list view
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */
   public function propertyList(Request $request)
   {
    try {
      // All property List
      $propertyList = Property::where('is_deleted', 0)->orderBy('id','DESC')->paginate(10); 
      return view('admin.property_list', ['property' => $propertyList]);
      
    } catch (Exception $e) {
      
    }

   }

   /**
     * Function Name : propertyView
     * Description   : load property details view
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return response
    */ 
   public function propertyView(Request $request)
   {
    try {
      // All property List
      $propertyDetails = Property::where('id', $request->id)->first(); 
      $propertyImages = PropertyImage::where('property_id', $request->id)->get(); 

      return view('admin.view_property', ['propertydetails' => $propertyDetails, 'propertyimages' => $propertyImages]);
      
    } catch (Exception $e) {
      
    }
   }
   
}
