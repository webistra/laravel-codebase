<?php

namespace App\Http\Controllers\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Aloha\Twilio\Twilio;
use Exception;

class Helper extends Controller
{
    /**
     * Send sms for mobile number, message
     *
     * @param string $to, message $message
     * @return 1
    */
    public static function sendSms($to, $message)
    {
        try 
        {
            //$accountSid = 'ACf571731a4913f05e172b8737338e2403';
            $accountSid = 'ACf786a64203b2524f8ee2878ee632bbe7';
            //$authToken = '64e48cea4342ecc3510d8a4ddad6e915';
            $authToken = '0f53e378507e1543cd5e2ddfcf5389a1';
            $twilioNumber = '+18555728559';
            $twilio = new Twilio($accountSid, $authToken, $twilioNumber);
            //$twilio->message('+919971410421', $message);
            $twilio->message($to, $message);
            return 1;
        }
        catch(Exception $e)
        {
            return $e->getMessage();
        } 
    }
}
