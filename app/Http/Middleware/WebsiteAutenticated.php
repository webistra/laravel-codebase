<?php

namespace App\Http\Middleware;
use App\UserRole;
use Closure;
use Redirect;

class WebsiteAutenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check())
        {
            $user_id = auth()->user()->id;
            
            $userRole = UserRole::where('user_id', $user_id)->first();

            if($userRole->role_id == 2){
                return $next($request);
            }else{
                 return Redirect::route('admin.dashboard');
            }
        }
        else
        {
           return $next($request);
        }
    }
}
