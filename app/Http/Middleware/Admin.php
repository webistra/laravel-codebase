<?php

namespace App\Http\Middleware;
use App\UserRole;
use Closure;
use Redirect;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check())
        {
            $user_id = auth()->user()->id;
           
            $userRole = UserRole::where('user_id', $user_id)->first();
           //print_r($user_id);exit;
            if($userRole->role_id == 1){
                return $next($request);
            }elseif($userRole->role_id == 2){
                 return Redirect::to('/');
            }else{
                 return Redirect::route('admin.login');
            }
        }
        else
        {
            return $next($request);
        }
    }
}
