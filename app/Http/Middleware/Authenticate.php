<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
//use Spatie\Permission\Traits\HasRoles;

use Closure;

class Authenticate
{
    //use HasRoles;
     /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;
    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role)
    {
        if(!$this->auth->check())
        {
            return redirect()->route('login')
                ->with('status', 'success')
                ->with('message', 'Please login.');
        }

        if (config('settings.activation')) {
            if ($this->auth->user()->activated == false) {
                session()->put('above-navbar-message', 'Please activate your email. <a href="'. route('authenticated.activation-resend') .'">Resend</a> activation email.');
            } else {
                session()->forget('above-navbar-message');
            }
        }
        $role = 'all';
        if($role == 'all')
        {
            return $next($request);
        }
        if( $this->auth->guest() || !$this->auth->user()->hasRole($role))
        {
            abort(403);
        }
        return $next($request);
    }
}
