<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppReview extends Model
{
    protected $table = 'app_review';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'rating',
    ];
}
