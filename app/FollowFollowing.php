<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowFollowing extends Model
{
    protected $table = 'follow_following';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'followed_to', 'followed_by',
    ];
}
