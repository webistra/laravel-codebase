<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use App\Role;
use App\UserRole;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens,HasRoles;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'gender','phone_number','country_code','password'
    ];


    public function hasRole($role)
    {
        return UserRole::where('user_id', Auth::user()->id)->get();
    }
}
