<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPasswordVerification extends Model
{
    protected $table = 'users_password_verification';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'accessToken',
    ];
}
