<?php

namespace App\Traits;

use App\Logic\Activation\ActivationRepository;
use App\User;
use Illuminate\Support\Facades\Validator;

trait ActivationTrait
{

    /**
     * Initiate email activation request
     *
     * @param string $user
     * @return 1
    */
    public function initiateEmailActivation(User $user)
    {

        if ( !config('settings.activation')  || !$this->validateEmail($user)) {

            return true;

        }

        $activationRepostory = new ActivationRepository();
        $activationRepostory->createTokenAndSendEmail($user);

    }

    /**
     * Validate email 
     *
     * @param string $user
     * @return 1
    */
    protected function validateEmail(User $user)
    {

        // Check does email posses valid format, cause if it's social account without
        // email, it'll have value of missingxxxxxxxxxx
        $validator = Validator::make(['email' => $user->email], ['email' => 'required|email']);

        if ($validator->fails()) {
            return false; // Stopping job execution, if it return false it will break entire application
        }

        return true;

    }

    /**
     * Resend activation email 
     *
     * @return 1
    */
    public function resend()
    {
        $this->initiateEmailActivation(auth()->user());

        return redirect()->route('home')
            ->with('status', 'success')
            ->with('message', 'Activation email sent.');
    }

}