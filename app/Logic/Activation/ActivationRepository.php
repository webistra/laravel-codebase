<?php

namespace App\Logic\Activation;

use App\User;
use Carbon;
use App\Notifications\SendActivationEmail;

class ActivationRepository
{
    /**
     * Create a random token for sending email
     *
     * @param string $user
     * @return 1
    */
    public function createTokenAndSendEmail(User $user)
    {
       
        // Limit number of activation attempts to 3 in 24 hours window
        $activations = User::where('id', $user->id)
            ->where('created_at', '>=', Carbon::now()->subHours(24))
            ->count();

        if ($activations >= 3) {
            return true;
        }

        if ($user->email_verified) { //if user changed activated email to new one

            $user->update([
                'email_verified' => false
            ]);

        }

        // Create new Activation record for this user/email

        $userFind = User::find($user->id);
        $userFind->remember_token = str_random(64);
        $userFind->update();

        // Send activation email notification
        $user->notify(new SendActivationEmail($userFind->remember_token));

    }

    /**
     * Delete expired activation from last 72 hours
     *
     * @return 1
    */
    public function deleteExpiredActivations()
    {

        User::where('created_at', '<=', Carbon::now()->subHours(72))->delete();

    }

}