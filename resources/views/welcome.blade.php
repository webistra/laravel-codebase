<!DOCTYPE html>
<html lang="en" class="no-js">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0,width=device-width,user-scalable=no">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Asset - Real Estate</title>
      <link rel="stylesheet" href="{{ asset('css/web-css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('css/fonts/stylesheet.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/style.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/mobile.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/animations.css') }}">
   </head>
   @if(auth()->check())
    <body>
   @else
    <body class="notLogged">
   @endif
      <header>
        <div class="header-inner">
         <div class="logo">
                <a href="{{ url('/') }}">
                  <img class="fill" src="{{ asset('img/logo.png') }}" alt="Logo"/>
                  <img class="trans" src="{{ asset('img/logo-w.png') }}" alt="Logo"/>
               </a>
          </div>
         <div class="container-fluid">
            <div class="row">
               <div class="col">
                  <span class="btn btn-mobmenu"><i class="fa fa-bars"></i></span>
                  <div class="top-left-menu menu">
               <ul>
                @if(auth()->check())
                  @if( auth()->user()->address_verified == 2 )
                    <li><a href="javascript:void(0);">What we do</a></li>
                    <li><a href="javascript:void(0);">How it works</a></li>
                    <li><a href="javascript:void(0);">Locations</a></li>
                    <li><a href="javascript:void(0);">Guides</a></li>
                    <li><a href="{{ route('login') }}">Join</a></li>
                  @endif
                @else
                    <li><a href="javascript:void(0);">What we do</a></li>
                    <li><a href="javascript:void(0);">How it works</a></li>
                    <li><a href="javascript:void(0);">Locations</a></li>
                    <li><a href="javascript:void(0);">Guides</a></li>
                    <li><a href="{{ route('login') }}">Join</a></li>
                @endif
                </ul>
              </div>
                  <div class="top-right-menu">
                   @if(!auth()->check())
                      <a class="btn btn-blue"  href="{{ route('login') }}">Sign In / Join</a>
                      <a href="javascript:;" class="btn btn-download btn-small blue-overlay"><span>Download the App</span></a>
                    @elseif( auth()->user()->address_verified == 2 )
                      <a class="btn btn-blue"  href="{{ route('settings') }}">Settings</a>   
                      <a class="btn btn-blue"  href="{{ route('logout') }}">Logout</a>
                    @else
                      <a class="btn btn-blue"  href="{{ route('logout') }}">Logout</a>
                   @endif
                 </div>
               </div>
            </div>
          </div>
         </div>
      </header>
      <div class="topBanner">
          <div class="topBanner-content d-flex align-items-center w-100">
             <div class="w-100">
                 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators custum-indicater">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					  <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
					  <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img class="d-block w-100" src="{{ asset('img/venice.png') }}" alt="First slide">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="{{ asset('img/NY.png') }}" alt="Second slide">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="{{ asset('img/SanFran.png') }}" alt="Third slide">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="{{ asset('img/London.png') }}" alt="Fourth slide">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="{{ asset('img/venice2.png') }}" alt="Fifth slide">
                      </div>
                    </div>
                  </div>
             </div>
             <div class="home-bannertext-box animatedParent">
                <h1 class="animated growIn slow">Community-Powered Property</h1>
                <div class="search-box-top animated fadeInUpShort">
                   <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <button class="btn btn-outline-secondary dropdown-toggle btn-dropdown-s" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Choose a City</button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Indai</a>
                        <a class="dropdown-item" href="#">Indai</a>
                        <a class="dropdown-item" href="#">Indai</a>
                          <a class="dropdown-item" href="#">Indai</a>
                          <a class="dropdown-item" href="#">Indai</a>
                      </div>
                    </div>
                    <input type="text" class="form-control" aria-label="Text input with dropdown button" placeholder="Search by keywords">
                    <div class="input-group-append">
                       <span class="input-group-text">
                          <img src="{{ asset('img/search-btn.png') }}">
                       </span>
                    </div>
                  </div>
                </div>
             </div>
          </div> 
      </div>
    
    <main class="main-container pdt0">
      <section class="assect-main common-section-main common-bg-block animatedParent" style="background:#fff;">
          <div class="container">
            <div class="row">
               <div class="col-12">
                   <div class="assest-block text-center animated fadeInDownShort">
                      <h3>Asset is a real estate marketplace which monetizes peer-to-peer referrals<br>
Leverage your own network of friends to crowdsource buyers & renters</h3>
                   </div>
              </div>
            </div>
          </div>
      </section>
      <section class="connect-friend-sec common-section-main common-bg-block animatedParent" style="background-image:url('{{ asset("img/bg1.png") }}')">
          <div class="container">
            <div class="row">
               <div class="col-12">
                   <div class="common-heading  text-center">
                      <h3>Help Friends Find a Home</h3>
                   </div>
                   <div class="connect-inner">
                      <div class="row align-items-center">
                         <div class="col-md-5 animated fadeInLeftShort">
                              <h5>Asset</h5>
                              <p>The world of Real Estate runs on word-of-mouth referrals. These referrals are often passed on to others
to facilitate & make a fee. With Asset you are rewarded when helping your friends find their new
home
                              </p>
                              <h5>Peer-to-Peer</h5>
                              <p>
                                Everyone in our community can introduce (tag) a friend to a property
                              </p>
                              <h5>Earn Money</h5>
                              <p>
                                Connect friends to property & earn money. An Intro Fee is paid upon a successful
transaction to the introducing party
                              </p>
                              <h5>Be an Influencer</h5>
                              <p>
                                Friends seek friends' advice on neighborhoods, buildings & local businesses to help
make a decision on their next move - Build & leverage your network
                              </p>
                              <h5>List</h5>
                              <p>
                                Rooms, Apartments or Houses
                              </p>
                         </div>
                         <div class="col-md-7 animated fadeInRightShort">
                            <img src="{{ asset('img/connent-friend.png') }}">
                         </div>
                      </div>
                   </div>
              </div>
            </div>
          </div>
      </section>
      <section class="rel-state-sec common-section-main common-bg-block " style="background-image:url('./images/bg4.png')">
          <div class="container">
            <div class="row">
               <div class="col-12">
                   <div class="real-state-inner ">
                      <div class="common-heading  text-center mb40">
                         <h3>Connecting People & Property</h3>
                       </div>
                       <div class="row">
                          <div class="col-md-6">
                             <div class="images-block h-100 d-flex align-items-center animatedParent">
                                <div id="carouselExampleFade" class="carousel slide carousel-fade animated fadeInUpShort" data-ride="carousel">
                                  <div class="carousel-inner">
                                    <div class="carousel-item active">
                                      <img class="d-block w-100" src="{{ asset('img/SLIDE_1.png') }}" alt="">
                                    </div>
                                    <div class="carousel-item">
                                      <img class="d-block w-100" src="{{ asset('img/SLIDE_2.png') }}" alt="Second slide">
                                    </div>
                                    <div class="carousel-item">
                                      <img class="d-block w-100" src="{{ asset('img/SLIDE_3.png') }}" alt="Third slide">
                                    </div>
                                    <div class="carousel-item">
                                      <img class="d-block w-100" src="{{ asset('img/SLIDE_4.png') }}" alt="Third slide">
                                    </div>
                                    <!-- <div class="carousel-item">
                                      <img class="d-block w-100" src="images/real-state-5.png" alt="Third slide">
                                    </div> -->
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>
                             </div>
                          </div>
                          <div class="col-md-6">
                              <div class="real-right-sec animatedParent"  data-sequence='2000'>
                                 <div class="real-text-block animated fadeInUp delay-250" data-id='1'>
                                   <div class="real-text-block-inner">
                                    <div>
                                       <h5>Property Listers</h5>
                                       <p>
                                          Landlords & Sellers now have a new way to market their properties. No need for costly
middlemen; manage your own listings & leverage online networks to crowd-source buyers &
renters.
                                       </p>
                                       <a class="btn btn-blue claim-btn">List My Property</a>
                                    </div>
                                   </div>
                                 </div>
                                 <div class="real-text-block second-block animated fadeInUp delay-500" data-id='1'>
                                   <div class="real-text-block-inner">
                                    <div>
                                       <h5>Property Seekers</h5>
                                       <p>
                                         Consumers trust their peers when it comes to recommendations; Word-of-mouth
marketing drives $6 Trillion in consumer spending annually. Buyers & Renters ;  Connect with our community to find your new home.
                                       </p>
                                       <a class="btn btn-blue claim-btn">Find a Home</a>
                                    </div>
                                   </div>
                                 </div>
                                 <div class="real-text-block animated fadeInUp delay-750" data-id='1'>
                                   <div class="real-text-block-inner">
                                    <div>
                                       <h5>Assest Community</h5>
                                       <p>
                                        Leverage your online networks to earn money by introducing
friends to properties listed on the Asset marketplace.
                                       </p>
                                       <a class="btn btn-blue claim-btn">Earn Money</a>
                                    </div>
                                   </div>
                                 </div>
                              </div>
                          </div>
                       </div>
                   </div>
              </div>
            </div>
          </div>
      </section>
      <section class="availablenear-sec common-section-main common-bg-block  animatedParent" style="background-image:url('{{ asset("img/bg5.png") }}')">
          <div class="container">
            <div class="row">
               <div class="col-12">
                   <div class="availablenear-block text-center  animated fadeInUpShort">
                      <div class="common-heading  text-center mb20">
                         <h3>Locations</h3>
                       </div>
                       <div class="availabl-image-block">
                          <div class="row">
                             <div class="col-md-4">
                                <div class="image-block-inner">
                                   <img src="{{ asset('img/new-york.png') }}" />
                                   <h3>New York</h3>
                                </div>
                             </div>
                             <div class="col-md-4">
                                 <div class="image-block-inner">
                                    <img src="{{ asset('img/los-angeles.png') }}" />
                                     <h3>Los Angeles</h3>
                                 </div>
                             </div>
                             <div class="col-md-4">
                                 <div class="image-block-inner">
                                    <img src="{{ asset('img/san-fransisco.png') }}" />
                                     <h3>San Francisco</h3>
                                 </div>
                             </div>
                          </div>
                       </div>
                   </div>
              </div>
            </div>
          </div>
      </section>
      <section class="neghbouirs-main common-section-main common-bg-block animatedParent" style="background-image:url('{{ asset("img/bg6.png") }}')">
          <div class="container">
            <div class="row">
               <div class="col-12">
                   <div class="neghbouirs-block text-center">
                      <div class="common-heading  text-center mb20  animated fadeInUpShort">
                         <h3>Around Town</h3>
                         <p>Follow locals & discover real insights on the best buildings, streets, cafes, businesses & more in
a neighborhood you’re considering…</p>
                       </div>
                       <div class="neghbours-inner">
                          <div class="row">
                             <div class="col-md-4">
                                <div class="neighbous-common-block">
                                   <div class="image-block">
                                      <iframe width="100%" height="315" src="https://www.youtube.com/embed/87CUTzGfaRQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                   </div>
                                   <div class="text-block mt10">
                                      <h5>Gabby & Shane</h5>
                                      <p>Brooklyn, New York</p>
                                   </div>
                                </div>
                             </div>
                             <div class="col-md-4">
                                <div class="neighbous-common-block">
                                   <div class="image-block">
                                      <iframe width="100%" height="315" src="https://www.youtube.com/embed/87CUTzGfaRQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                   </div>
                                   <div class="text-block mt10">
                                      <h5>Gabby & Shane</h5>
                                      <p>Brooklyn, New York</p>
                                   </div>
                                </div>
                             </div>
                             <div class="col-md-4">
                                <div class="neighbous-common-block">
                                   <div class="image-block">
                                      <iframe width="100%" height="315" src="https://www.youtube.com/embed/87CUTzGfaRQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                   </div>
                                   <div class="text-block mt10">
                                      <h5>Gabby & Shane</h5>
                                      <p>Brooklyn, New York</p>
                                   </div>
                                </div>
                             </div>
                             <div class="col-md-4">
                                <div class="neighbous-common-block">
                                   <div class="image-block">
                                     <iframe width="100%" height="315" src="https://www.youtube.com/embed/87CUTzGfaRQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                   </div>
                                   <div class="text-block mt10">
                                      <h5>Gabby & Shane</h5>
                                      <p>Brooklyn, New York</p>
                                   </div>
                                </div>
                             </div>
                             <div class="col-md-4">
                                <div class="neighbous-common-block">
                                   <div class="image-block">
                                      <iframe width="100%" height="315" src="https://www.youtube.com/embed/87CUTzGfaRQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                   </div>
                                   <div class="text-block mt10">
                                      <h5>Gabby & Shane</h5>
                                      <p>Brooklyn, New York</p>
                                   </div>
                                </div>
                             </div>
                             <div class="col-md-4">
                                <div class="neighbous-common-block">
                                   <div class="image-block">
                                      <iframe width="100%" height="315" src="https://www.youtube.com/embed/87CUTzGfaRQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                   </div>
                                   <div class="text-block mt10">
                                      <h5>Gabby & Shane</h5>
                                      <p>Brooklyn, New York</p>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                   </div>
              </div>
            </div>
          </div>
      </section>
      <section class="download-sec common-section-main common-bg-block " style="background-image:url('{{ asset("img/bg7.png") }}')">
          <div class="container">
            <div class="common-heading text-center  mb30">
               <h3>Connect with the Asset Community</h3>
               <h4>We will be launching in your city very soon. Please sign up to be first in line when we do!</h4>
            </div>
            <div class="row">
               <div class="col-12">
                   <div class="download-sec animatedParent">
                       <div class="row align-items-center">
                          <div class="col-md-6 ">
                             <div class="image-section  animated fadeInLeft slow">
                                <img src="{{ asset('img/mobile-screen.png') }}">
                             </div>
                          </div>
                          <div class="col-md-6">
                             <div class="right-text-section text-center animated fadeInRight slow">
                                
                                <button class="btn btn-downloadapp">Download the App</button>
                                 <form class="join-form">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control"  placeholder="Enter email address..">
                                        <div class="input-group-append">
                                           <button class="btn btn-black"><i class="fa fa-angle-right"></i></button>
                                        </div>
                                    </div>
                                 </form>
                             </div>
                          </div>
                       </div>
                   </div>
            </div>
          </div>
      </section>
    </main>
    <footer class="text-center pt20">
       <div class="container-footer">
          <div class="top-footer">
             <div class="footer-link">
                <ul>
                   <li><a href="contact-us.html">Contact Us</a></li>
                   <li><a href="about-us.html">About Us</a></li>
                   <li><a href="terms.html">Terms & Conditions</a></li>
                   <li><a href="privacy.html">Privacy Policy</a></li>
                   <li><a href="carrer.html">Careers</a></li>
                   <li><a href="faq.html">FAQ</a></li>
                   <li><a href="partner.html">Partnerships</a></li>
                </ul>
             </div>
              <ul class="socialLinks mb15 text-center">
                   <li><a href="javascript:void(0);" class="fbBg"><i class="fab fa-facebook-f"></i></a></li>
                   <li><a href="javascript:void(0);" class="twitterBg"><i class="fab fa-twitter"></i></a></li>
                   <li><a href="javascript:;" class="instagramBg"><i class="fab fa-instagram"></i></a></li>
                </ul>
          </div>
          <div class="bottom-fotter">
             <p>All Rights Reserved © AssetDotCo Inc</p>
          </div>
       </div>
    </footer>
      <script src="{{ asset('js/web-scripts/jquery-3.3.1.min.js')}}"></script>
      <script src="{{ asset('js/web-scripts/popper.min.js')}}"></script>
      <script src="{{ asset('js/web-scripts/bootstrap.min.js')}}"></script>
      <script src="{{ asset('js/web-scripts/common.js')}}"></script>
      <script src="{{ asset('js/web-scripts/css3-animate-it.js')}}"></script>
      <script type="text/javascript">
        $('.carousel').carousel({
          interval:3000,
          cycle: true
        })
      </script>
   </body>
</html>