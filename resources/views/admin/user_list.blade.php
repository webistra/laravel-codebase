@extends('layouts.admin')

@section('content')
        <main class="middle-content">
            <!-- Page Title Start -->
            <div class="page_title_block">
                <h1 class="page_title">User Management</h1>
               
            </div>
            <!-- Page Title End -->
            <div class="content-section">
                <div class="outer-box">

                    <!-- Gloabl Table Box Start -->
                    <div class="global-table no-radius p0">

                        <div class="tab-content">                          
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="no_wrap_th">
                                                <th>Name</th>
                                                <th>Email ID</th>
                                                <th>Mobile Number</th>
                                                <th>Created Date</th>
                                                <th>User Status</th>
                                                <th class="action_td_btn3">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($users as $value)
                                            <tr>
                                              
                                                <td>{{ $value->name }}</td>
                                                <td>{{ $value->email }}</td>
                                                <td>{{ $value->country_code.$value->phone_number }}</td>
                                                 <td>{{ date('m-d-Y',strtotime($value->created_at)) }}</td>
                                                 @if($value->is_blocked)
                                                <td>Blocked </td>
                                                  @else
                                                <td>Active</td>
                                                @endif

                                                <td class="action_td_btn3">
                                                    <a href="{{ route('admin.user.view',$value->id) }}"  class="btn btn-info btn-raised">View</a>
                                                    @if($value->is_blocked)
                                                    <a onclick="activateDeactivateUser('{{$value->id}}','{{ 0 }}','User unblocked successfully.')" class="btn btn-danger"> <i class="text-white fa fa-ban" aria-hidden="true"> Blocked</i></a>
                                                    @else
                                                    <a onclick="activateDeactivateUser('{{$value->id}}','{{ 1 }}','User blocked successfully.')" class="btn btn-success"> <i class=" text-white fa fa-unlock-alt"> Activate</i></a>
                                                    @endif
                                                     <a onclick="activateDelete('{{$value->id}}','User Deleted successfully.')" class="btn btn-danger btn-raised">  Delete</a>
                                                </td>
                                            </tr>
                                            @empty
                                                <tr class="row">
                                                    <td>
                                                        {{ 'No Data Found!' }}
                                                    </td>
                                                </tr>
                                            @endforelse

                                        </tbody>
                                    </table>
                                </div>
                                <div class="custom-pagination mt20 text-center">
                                  {{ $users->links() }}
                                </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Table Responsive End -->
    </div>
    </main>
   
    </div>
    <!-- Wrapper End -->

@endsection
@section('scripts')

<script type="text/javascript">
   var public_url = $('meta[name="base_url"]').attr('content');


var params = window.location.search;


  function activateDeactivateUser(id,status,msg) {

 Swal({
  title: 'Are you sure?',
  text: 'Do you want to perform this action ?',
  type: 'warning',
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  showCancelButton: true,
  confirmButtonText: 'Yes',
  cancelButtonText: 'No'
}).then((result) => {
  if (result.value) {
         $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
     $.ajax({
        method: 'POST',
        data: {
          'id': id,
          'status':status
        },
        url: public_url + "/admin/user/active-deactive",
        success: function(data) {
            
          swal({
              title: "Done",
              text: msg,
              type: "success",
              //timer: 300000000,
            },
            );
          $('.swal2-confirm').click(function(){
                window.location.href = public_url + "/admin/user/list";
          });
        }
      })
  } else if (result.dismiss === Swal.DismissReason.cancel) {
    Swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})

  }

    function activateDelete(id,msg) {

 Swal({
  title: 'Are you sure?',
  text: 'Do you want to perform this action ?',
  type: 'warning',
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  showCancelButton: true,
  confirmButtonText: 'Yes',
  cancelButtonText: 'No'
}).then((result) => {
  if (result.value) {
         $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
     $.ajax({
        method: 'POST',
        data: {
          'id': id        
        },
        url: public_url + "/admin/user/delete",
        success: function(data) {
            
          swal({
              title: "Done",
              text: msg,
              type: "success",
              //timer: 300000000,
            },
            );
          $('.swal2-confirm').click(function(){
                window.location.href = public_url + "/admin/user/list";
          });
        }
      })
  } else if (result.dismiss === Swal.DismissReason.cancel) {
    Swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})

  }
   
</script>>
@endsection