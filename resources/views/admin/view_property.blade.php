@extends('layouts.admin')

@section('stylesheet')
<style type="text/css">
	/*
A super simple jQuery slider plugin

Table of contents

1.0 - Typography
2.0 - Elements
3.0 - Grid
4.0 - Slider
	4.1 - Slider Nav
*/


/*
1.0 - Typography
*/

h1,
h2 {
    font-family: Candal, sans-serif;
    line-height: 1.2;
}


/*
2.0 - Elements
*/

img {
    max-width: 100%;
    max-height:100%;height:100%;width:100%;
    
}

.slider__item{height:500px;}
/*
3.0 - Grid
*/

.grid {
    max-width: 960px;
    margin: auto;
}


/*
4.0 - Slider
*/

.slider-box,
.slider__item {
    position: relative;
}

.slider {
    overflow: hidden;
}

.slider__canvas {
    transition: transform 0.5s;
}

.slider__item {
    float: left;
}

.slider__item__title {
    opacity: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    width: 70%;
    margin: 0;
    color: #468;
    font-size: 2.5rem;
    line-height: 1;
    text-align: center;
    text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.25);
    transition: opacity 0.3s, transform 0.3s;
    transform: translate3d(-50%, -60%, 0);
}

.active .slider__item__title {
    opacity: 1;
    transition-delay: 0.5s;
    transform: translate3d(-50%, -50%, 0);
}

@media(min-width: 48rem) {
    .slider__item__title {
        font-size: 5rem;
    }
}


/*
4.1 - Slider Nav
*/

.slider-nav {
    color: #fff;
    font-size: 1rem;
    text-align: center;
}

.slider-nav__dots {
    position: absolute;
    bottom: 20px;
    left: 20px;
    right: 20px;
}

.slider-nav__prev,
.slider-nav__next,
.slider__dot {
    border-radius: 50%;
    box-shadow: inset 0 0 0 2px;
    cursor: pointer;
    backface-visibility: hidden;
    transition: transform 0.3s, box-shadow 0.3s;
}

.slider-nav__prev,
.slider-nav__next {
    position: absolute;
    top: 50%;
    width: 3rem;
    height: 3rem;
    margin-top: -1.5rem;
    line-height: 3rem;
}

.slider-nav__prev {
    left: 7%;
}

.slider-nav__next {
    right: 7%;
}

.slider__dot {
    display: inline-block;
    width: 1rem;
    height: 1rem;
    margin: 0 1rem;
}

.slider-nav__prev:hover,
.slider-nav__next:hover,
.slider__dot.active,
.slider__dot:hover {
    transform: scale3d(1.5, 1.5, 1);

}
.image-profile-user {
    max-width: 100%;
    border-radius: 11%;
}
</style>
@endsection
@section('content')
        <main class="middle-content">
            <!-- Page Title Start -->
            <div class="page_title_block">
                <h1 class="page_title">View Property</h1>
               
            </div>
            <!-- Page Title End -->
            <div class="content-section">

<!--  -->
@if($propertyimages)
<div class="grid">
    <div class="slider-box">
        <div class="slider" id="slider">
        	@foreach($propertyimages as $value)
            <div class="slider__item">
                <img  src="{{ asset('storage/'.$value->images) }}" alt="">
            </div>
            @endforeach
            
        </div>

        <div class="slider-nav">
            <div class="slider-nav__prev" id="prev"><i class="fa fa-angle-left"></i></div>
            <div class="slider-nav__next" id="next"><i class="fa fa-angle-right"></i></div>
            <div class="slider-nav__dots" id="dots"></div>
        </div>
    </div>
</div>
@endif
<!--  -->
                <div class="order-view mt30 max-WT-700 mrgn-0-auto">
                    <div class="main-block-innner mb40 mt40">
                        <div class="add-store-block input-style">
                            <div class="form-group row align-items-top">
                                <label class="col-md-5">Property main Image :</label>
                                <div class="col-md-7">
                                    @if($propertydetails->main_image)
                                        <img class="img-circle image image-profile-user" src="{{ asset('storage/'.$propertydetails->main_image) }}">
                                    @else
                                        <img class="img-circle image image-profile-user" src="{{ asset('storage/uploads/profile_images/default_image.png') }}">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Property Name :</label>
                                <div class="col-md-7">
                                    <label>{{ $propertydetails->property_name }}</label>
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Property Address :</label>
                                <div class="col-md-7">
                                    <label> {{ $propertydetails->property_address }} </label>
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Zip Code :</label>
                                <div class="col-md-7">
                                    <label>{{ $propertydetails->zip_code }}</label>
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Property Type :</label>
                                <div class="col-md-7">
                                	 @if($propertydetails->property_type == 1)
                                    	<label class="badge badge-success">Apartment</label>
                                     @elseif($propertydetails->property_type == 2)
                                    	<label class="badge badge-success">House</label>
                                    @else
                                    	<label class="badge badge-success">Room</label>
                                     @endif
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Number of Bedrooms :</label>
                                <div class="col-md-7">
                                    <label>{{$propertydetails->no_of_bedrooms }}</label>
                                </div>
                            </div>
                              <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Number of bathroom :</label>
                                <div class="col-md-7">
                                    <label>{{ $propertydetails->no_of_bathrooms	 }}</label>
                                </div>
                            </div>
                              <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Other living room :</label>
                                <div class="col-md-7">
                                    <label>{{ $propertydetails->other_living_room }}</label>
                                </div>
                            </div>
                              <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Parking Garage :</label>
                                <div class="col-md-7">
                                    <label>{{ $propertydetails->parking_garage }}</label>
                                </div>
                            </div>
                               <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Preffered Suburb :</label>
                                <div class="col-md-7">
                                    <label>{{ $propertydetails->preffered_suburb }}</label>
                                </div>
                            </div>
                             <div class="form-group row align-items-top">
                                <label class="col-md-5">Description :</label>
                                <div class="col-md-7">
                                    <label>{{ $propertydetails->description }}</label>
                                </div>
                            </div>
                             <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Price :</label>
                                <div class="col-md-7">
                                    <label>{{ $propertydetails->price_for_property }}</label>
                                </div>
                            </div>
                             <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Pet allowed :</label>
                                <div class="col-md-7">
                                	@if($propertydetails->is_pet_allowed)
                                    <label>Yes</label>
                                    @else
                                    <label>No</label>
                                    @endif
                                </div>
                            </div>

                            <div class="text-left mt40">
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
       
@endsection
@section('scripts')

<script type="text/javascript">
	(function($) {

    'use strict';

    var pluginName = 'slider',
        defaults = {
            next: '.slider-nav__next',
            prev: '.slider-nav__prev',
            item: '.slider__item',
            dots: false,
            dotClass: 'slider__dot',
            autoplay: false,
            autoplayTime: 3000,
        };

    function slider(element, options) {
        this.$document = $(document);
        this.$window = $(window);
        this.$element = $(element);
        this.options = $.extend({}, defaults, options);
        this.init();
    };

    slider.prototype.init = function() {
        this.setup();
        this.attachEventHandlers();
        this.update();
    };

    slider.prototype.setup = function(argument) {
        this.$slides = this.$element.find(this.options.item);
        this.count = this.$slides.length;
        this.index = 0;

        this.$next = $(this.options.next);
        this.$prev = $(this.options.prev);

        this.$canvas = $(document.createElement('div'));
        this.$canvas.addClass('slider__canvas').appendTo(this.$element);
        this.$slides.appendTo(this.$canvas);

        this.$dots = $(this.options.dots);
        this.$dots.length && this.createDots();
    };

    slider.prototype.createDots = function() {
        var dots = [];
        for (var i = 0; i < this.count; i += 1) {
            dots[i] = '<span data-index="' + i + '" class="' + this.options.dotClass + '"></span>';
        }
        this.$dots.append(dots);
    }

    slider.prototype.attachEventHandlers = function() {
        this.$element.on('prev.slider', this.prev.bind(this));
        this.$document.on('click', this.options.prev, (function(e) {
            this.$element.trigger('prev.slider');
        }).bind(this));

        this.$element.on('next.slider', this.next.bind(this));
        this.$document.on('click', this.options.next, (function(e) {
            this.$element.trigger('next.slider');
        }).bind(this));

        this.$element.on('update.slider', this.update.bind(this));
        this.$window.on('resize load', (function(e) {
            this.$element.trigger('update.slider');
        }).bind(this));

        this.$element.on('jump.slider', this.jump.bind(this));
        this.$document.on('click', ('.' + this.options.dotClass), (function(e) {
            var index = parseInt($(e.target).attr('data-index'));
            this.$element.trigger('jump.slider', index);
        }).bind(this));

        this.$element.on('autoplay.slider', this.autoplay.bind(this));
        this.$element.on('autoplayOn.slider', this.autoplayOn.bind(this));
        this.$element.on('autoplayOff.slider', this.autoplayOff.bind(this));
        this.$element.bind('prev.slider next.slider jump.slider', this.autoplay.bind(this));
        this.options.autoplay && this.$element.trigger('autoplayOn.slider');
    };

    slider.prototype.next = function(e) {
        this.index = (this.index + 1) % this.count;
        this.slide();
    };

    slider.prototype.prev = function(e) {
        this.index = Math.abs(this.index - 1 + this.count) % this.count;
        this.slide();
    };

    slider.prototype.jump = function(e, index) {
        this.index = index % this.count;
        this.slide();
    }

    slider.prototype.autoplayOn = function(argument) {
        this.options.autoplay = true;
        this.$element.trigger('autoplay.slider');
    };

    slider.prototype.autoplayOff = function() {
        this.autoplayClear();
        this.options.autoplay = false;
    }

    slider.prototype.autoplay = function(argument) {
        this.autoplayClear();
        if (this.options.autoplay) {
            this.autoplayId = setTimeout((function() {
                this.$element.trigger('next.slider');
                this.$element.trigger('autoplay.slider');
            }).bind(this), this.options.autoplayTime);
        }
    };

    slider.prototype.autoplayClear = function() {
        this.autoplayId && clearTimeout(this.autoplayId);
    }

    slider.prototype.slide = function(index) {
        undefined == index && (index = this.index);
        var position = index * this.width * -1;
        this.$canvas.css({
            'transform': 'translate3d(' + position + 'px, 0, 0)',
        });
        this.updateCssClass();
    };

    slider.prototype.update = function() {
        this.width = this.$element.width();
        this.$canvas.width(this.width * this.count);
        this.$slides.width(this.width);
        this.slide();
    };

    slider.prototype.updateCssClass = function() {
        this.$slides
            .removeClass('active')
            .eq(this.index)
            .addClass('active');

        this.$dots
            .find('.' + this.options.dotClass)
            .removeClass('active')
            .eq(this.index)
            .addClass('active');
    }

    $.fn[pluginName] = function(options) {
        return this.each(function() {
            !$.data(this, pluginName) && $.data(this, pluginName, new slider(this, options));
        });
    };

})(window.jQuery);

$('#slider').slider({
    prev: '#prev',
    next: '#next',
    dots: '#dots',
    autoplay: true,
});

</script>

@endsection