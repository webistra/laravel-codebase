@extends('layouts.admin')

@section('content')
        <main class="middle-content">
            <!-- Page Title Start -->
            <div class="page_title_block">
                <h1 class="page_title">View Document</h1>
               
            </div>
            <!-- Page Title End -->
            <div class="content-section">

                <div class="order-view mt30 max-WT-700 mrgn-0-auto">
                    @if (session('status'))
                          <div class="alert alert-success  alert-dismissible">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          {{ session('status') }}
                          </div>
                    @endif
                    <div class="main-block-innner mb40 mt40">
                        <div class="add-store-block input-style">
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5"><b>Document Image :</b></label>
                                <div class="col-md-7">
                                    @if($user->documents)
                                        <img class=" image r" src="{{ asset('storage/'.$user->documents) }}">
                                    @else
                                        <img class="img-circle image image-profile-user" src="{{ asset('storage/uploads/profile_images/default_image.png') }}">
                                    @endif
                                </div>
                            </div>
                            <div class="text-center">
                              <a href="{{ route('admin.upload.user.approve',$user->id) }}" class="btn btn-info ">Approve</a>
                            </div>
                            <div class="text-left mt40">
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
       
@endsection