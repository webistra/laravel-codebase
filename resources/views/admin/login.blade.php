<!DOCTYPE html>
<head>
    <link rel="stylesheet" href="{{asset('css/admin-css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/fonts/stylesheet.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin-css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin-css/style_s.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin-css/mobile.css')}}">
  <title>Asset-Real Estate</title>
</head>
<body class="afterlogin">
  <div class="login-wrapper">
    <div class="container-common">
       <div class="row">
       <div class="col-md-6">
          <div class="left-section-login">
              <div class="login-logo1">
                 <img src="img/logo-w.png" alt="logo" />
              </div>
              <div class="sigintext-block">
              <h2>Sign in to your account</h2>
              <p>Community-Powered Property.</p>
            </div>
          </div>
       </div>
      <div class="col-md-6">
        <form class="login_box_outer" id="login" method="POST" action="{{ route('index') }}"  ">
          @csrf
          <div class="login-box max-WT-520">
              <div class="login-right-block">
                <div class="login-heading">
                   <h4>SIGN IN</h4>
                </div>
                <div class="login-box-body">
                  <div class="form-group">
                    <label class="common-label blue">Sign in e-mail</label>
                      <input type="text" name="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter your email id" required autofocus maxlength="60"/>
                       @if ($errors->has('email'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="common-label blue">Sign in password</label>
                      <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}  " placeholder="Enter your password" required  maxlength="16"/>
                      @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                      @endif
                    </div>
                    <div class="form-group">
                     <!--    <div class="col-6">
                          <div class="remember-text ">
                            <label class="checkbox-design">
                              <input type="checkbox"/><span></span>Remember me
                            </label>
                          </div>
                        </div> -->
                        <div class="text-right">
                           <a href="forgot-password.html" class="forget-pass">Forgot Password?</a>
                        </div>
                        <!-- <div class="col-lg-6">
                          <div class="forgot-links">
                            <a href="forgot.html">Forgot Password?</a>
                          </div>
                        </div> -->
                    </div>
                  </div>
                    <div class="text-center mt40">
                      <button  type="submit" class="btn btn-login btn-large  width100 font-100">LOGIN</button>
                    </div>
              </div>
          </div>
        </form>
      </div>
    </div>
    </div>
  </div>
 <footer class="before-login">
    <p>© Asset. Co. Limited 2018-2019. All rights reserved.</p>
 </footer>
 <script src="{{ asset('js/admin-script/jquery-3.3.1.min.js')}}"></script>
 <script src="{{ asset('/js/jquery.validate.min.js') }}" ></script>
<script src="{{ asset('js/admin-script/popper.min.js')}}"></script>
<script src="{{ asset('js/admin-script/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/admin-script/jquery.nicescroll.min.js')}}"></script>
<script src="{{ asset('js/admin-script/custom.js')}}"></script>

</body>
</html>


<script type="text/javascript">
  $('#login').validate({
    focusInvalid: false,
    ignore: [],
    rules: {
        email: {
          required: true,
           remote: {
            url : "{{ route('valid-email')}}",
            type : "GET",
            dataFilter: function (data) {
                    var json = JSON.parse(data);
                    console.log(json)
                    if (json)
                        return 'false';
                    else 
                        return 'true';
                },    
            },
        },
      password: {
        required: true
      }
    },
    messages: {
      email: {
        required: 'Please enter email address.',
        remote:  'Email address does not exists.'
      },
      password: {
        required: 'Please enter password.',
        
      }
    },
    submitHandler: function(form) {
      form.submit();
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") == "email" || element.attr("name") == "password" )
            $('.invalid-feedback').remove();
            error.insertAfter(element);
    }
  });
</script>

