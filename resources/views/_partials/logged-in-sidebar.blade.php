<!-- LeftMenuBox -->
<div class="leftMenuBox">
  <div class="topSearchBox">
     <form>
        <input class="form-control" type="text" placeholder="Search"/>
        <button type="submit" class="btnTopSearch"><i class="fas fa-search"></i></button>
     </form>
  </div>
  <!--Accordion Start -->
  <div id="accordion" class="sidebar-menu">
     <!-- menu-box Start -->
     <div class="menu-box active">
        <div class="card-header clearfix" id="headingOne">
           <a class="side_menu" href="{{ route('notice-board') }}">
           <span class="side_menu_icon"><img src="{{ asset('img/notice-board.png') }}" alt="Icon"/></span><span>Notice Board</span>
           </a>
        </div>
     </div>
     <!-- menu-box End -->
     <!-- menu-box Start -->
     <div class="menu-box toggle-menu">
        <div class="card-header clearfix" id="headingThree">
           <a class="side_menu"  href="my-property.html">
           <span class="side_menu_icon"><img src="{{ asset('img/home.png') }}" alt="Icon"/></span>My Property
           </a>
           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
           </button>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
           <div class="card-body">
              <ul class="inner-submenu">
                 <li><a href="all-property.html">All Property</a></li>
                 <li><a href="my-property.html">For Sale</a></li>
                 <li><a href="my-property.html">For Rent</a></li>
                 <li><a href="property-details.html">Property Details</a></li>
                 <li class="dropdown accordDropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"  aria-expanded="false">Add New</a>
                    <div class="dropdown-menu">
                       <a class="dropdown-item" href="sell-property-form.html">For Sell Form</a>
                       <a class="dropdown-item" href="rent-property-form.html">For Rent Form</a>
                    </div>
                 </li>
              </ul>
           </div>
        </div>
     </div>
     <!-- menu-box End -->
     <!-- menu-box Start -->
     <div class="menu-box">
        <div class="card-header clearfix" id="headingFour">
           <a class="side_menu" href="search-property.html">
           <span class="side_menu_icon"><img src="{{ asset('img/search.png') }}" alt="Icon"/></span><span>Search Property</span>
           </a>
        </div>
     </div>
     <!-- menu-box End -->
     <!-- menu-box Start -->
     <div class="menu-box">
        <div class="card-header clearfix" id="headingFour">
           <a class="side_menu" href="following-property.html">
           <span class="side_menu_icon"><img src="{{ asset('img/follow.png') }}" alt="Icon"/></span><span>Following Property</span>
           </a>
        </div>
     </div>
     <!-- menu-box End -->
     <!-- menu-box Start -->
     <div class="menu-box">
        <div class="card-header clearfix" id="headingFour">
           <a class="side_menu" href="community.html">
           <span class="side_menu_icon"><img src="{{ asset('img/community.png') }}" alt="Icon"/></span><span>Community</span>
           </a>
        </div>
     </div>
     <!-- menu-box End -->
     <!-- menu-box Start -->
     <div class="menu-box">
        <div class="card-header clearfix" id="headingFour">
           <a class="side_menu" href="introduction.html">
           <span class="side_menu_icon"><img src="{{ asset('img/introduction.png') }}" alt="Icon"/></span><span>Introduction/Tags</span>
           </a>
        </div>
     </div>
     <!-- menu-box End -->
     <!-- menu-box Start -->
     <div class="menu-box">
        <div class="card-header clearfix" id="headingFour">
           <a class="side_menu" href="bank.html">
           <span class="side_menu_icon"><img src="{{ asset('img/bank-building.png') }}" alt="Icon"/></span><span>Bank</span>
           </a>
        </div>
     </div>
     <!-- menu-box End -->
  </div>
  <!--Accordion End -->
</div>
<!-- LeftMenuBox End-->