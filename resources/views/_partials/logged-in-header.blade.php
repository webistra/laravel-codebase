<header class="inner-page-header">
   <div class="header-inner">
      <div class="logo">
         <a href="{{ route('notice-board') }}">
         <img src="{{ asset('img/logo-w.png') }}" alt="Logo"/>
         </a>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col">
               <span class="btn btn-mobmenu"><i class="fa fa-bars"></i></span>
               <div class="top-left-menu menu">
                  <div class="topSearchBox">
                     <form>
                        <input class="form-control" type="text" placeholder="Search"/>
                        <button type="submit" class="btnTopSearch"><i class="fas fa-search"></i></button>
                     </form>
                  </div>
                  <a href="add-new-property.html" class="btn btnWhite"><span>Add new property</span></a>
               </div>
               <div class="top-right-menu">
                  <a href="chat.html" class="notifyIcon"><i class="fas fa-comments"></i></a>
                  <a href="notification.html" class="notifyIcon"><i class="fas fa-bell"></i></a>
                  <div class="user_top_box clearfix">
                     <img src="{{ asset('img/profile-img.png') }}" alt="user image"/>
                     <div class="dropdown-menu head-drop-down">
                        <a class="dropdown-item" href="{{ route('profile') }}">Profile</a>
                        <a class="dropdown-item" href="{{ route('user-change-password') }}">Change Password</a>
                        <a class="dropdown-item" href="#logoutModal" data-toggle="modal" data-target="#logoutModal">Logout</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</header>
<!-- Header End -->