  <header class="header">
            <nav class="navbar clearfix">
                <div class="logo-box">
                    <a class="navbar-brand logo" href="{{ route('admin.dashboard') }}">
                        <img src="{{ asset('admin-asset/img/logo.png') }}" alt="logo" class="lg-logo admin-logo">
                        <!-- <img src="img/sm-logo.png" alt="logo" class="sm-logo"> -->
                    </a>

                </div>
                <div class="header-right-part">
                    <button class="btn btn-toggle" type="button">
                      <i class="fas fa-bars"></i>
                    </button>
                    <button class="btn btn-outline-secondary btn-mobsearch" type="button"><i class="fas fa-search"></i></button>
                   
                </div>
                <div class="nav-right-box">
                    <div class="acknowledge-box">
                       
                       
                    </div>
                    <div class="head_user-box clearfix">
                        <div class="head_user_name clearfix">
                            <span class="user_name">{{auth()->user()->name }}</span>
                            <div class="top-user-img">
                                <img src="{{asset('storage/'.auth()->user()->profile_image) }}" alt="User Image">
                                <div class="dropdown-menu head-drop-down">
                                    <a class="dropdown-item" href="{{ route('user.view') }}">My Profile</a>
                                    <a class="dropdown-item" href="change-password.html">Change Password</a>
                                     <a class="dropdown-item" href="{{ route('admin.logout') }}">Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <span class="btn-zoom"><i class="fas fa-expand"></i></span> -->
                </div>
            </nav>
        </header>