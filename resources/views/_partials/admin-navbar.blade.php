@php
    $segment = request()->segment(2);
@endphp
 <aside class="sidebar">
            <div class="sidebar-scroller">
                <!--Accordion Start -->
                <div id="accordion" class="sidebar-menu">
                    <div class="user-panel clearfix">
                        <div class="pull-left image">
                            <img src="{{asset('storage/'.auth()->user()->profile_image) }}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info mt10">
                          
                            <p>Welcome</p>
                            <h4>{{auth()->user()->name }}</h4>
                         
                        </div>
                    </div>
                    <!-- menu-box Start -->
                   <div class="menu-box {{ ($segment == 'dashboard')? 'active':'' }}">
                        <div class="card-header clearfix" id="headingOne">
                            <a class="side_menu" href="{{ route('admin.dashboard') }}">
                                <span class="side_menu_icon"><!-- <i class="fas fa-tachometer-alt"></i> -->
                                    <img src="{{ asset('admin-asset/img/dashboard-icon.png') }}" alt="icon"/>
                                </span>
                                <span class="side_page_name">Dashboard</span>
                            </a>
                        </div>
                    </div>
                    <!-- menu-box End -->
                    <!-- menu-box Start -->
                   
                    <div class="menu-box {{ ($segment == 'user')? 'active':'' }}">
                        <div class="card-header clearfix" id="headingTwo">
                            <a class="side_menu" href="{{ route('admin.user.list') }}">
                                <span class="side_menu_icon">
                                    <img src="{{ asset('admin-asset/img/user-manage.png') }}" alt="icon"/>
                                </span>
                                <span class="side_page_name">User Management</span>
                            </a>
                        </div>
                    </div>
                    <!-- menu-box End -->
                    <!-- menu-box Start -->
                   <div class="menu-box {{ ($segment == 'property')? 'active':'' }}">
                        <div class="card-header clearfix" id="headingThree">
                               <a class="side_menu" href="{{ route('admin.property.list') }}">
                                <span class="side_menu_icon">
                                    <img src="{{ asset('admin-asset/img/pay-manage.png') }}" alt="icon"/>
                                </span>
                                <span class="side_page_name">Property Management</span>
                            </a>
                        </div>
                    </div>
                    <!-- menu-box End -->
                    <!-- menu-box Start -->
                    <div class="menu-box {{ ($segment == 'upload')? 'active':'' }}">
                        <div class="card-header clearfix" id="headingFour">
                          <a class="side_menu" href="{{ route('admin.upload.user.list') }}">
                                <span class="side_menu_icon">
                                    <img src="{{ asset('admin-asset/img/play-button.png') }}" alt="icon"/>
                                </span>
                                <span class="side_page_name">Document Management</span>
                            </a>
                        </div>
                    </div>
                    <!-- menu-box End -->
                    <!-- menu-box Start -->
                  <!--   <div class="menu-box">
                        <div class="card-header clearfix" id="headingFour">
                            <a class="side_menu" href="firm-upgradation.html">
                                <span class="side_menu_icon">
                                     <img src="img/copy-content.png" alt="icon"/>
                                 </span>
                                <span class="side_page_name">Firmare upgradation</span>
                            </a>
                        </div>
                    </div> -->
                    <!-- menu-box End -->
                    <!-- menu-box Start -->
                  <!--   <div class="menu-box">
                        <div class="card-header clearfix" id="headingFour">
                            <a class="side_menu" href="summary.html">
                                <span class="side_menu_icon">
                                    <img src="img/category-manage.png" alt="icon"/>
                                </span>
                                <span class="side_page_name">Summary</span>
                            </a>
                        </div>
                    </div> -->
                    <!-- menu-box End -->
                    <!-- menu-box Start -->
                  <!--   <div class="menu-box">
                        <div class="card-header clearfix" id="headingFour">
                            <a class="side_menu" href="term-condition.html">
                                <span class="side_menu_icon">
                                    <img src="img/static-content.png" alt="icon"/>
                                </span>
                                <span class="side_page_name">Static Content</span>
                            </a>
                        </div>
                    </div> -->
                    <!-- menu-box End -->
                    <!-- menu-box Start -->
                   <!--  <div class="menu-box">
                        <div class="card-header clearfix">
                            <a class="side_menu" href="#signout_modal"  data-toggle="modal">
                                <span class="side_menu_icon">
                                    <img src="img/logout.png" alt="icon"/>
                                </span>
                                <span class="side_page_name">Logout</span>
                            </a>
                        </div>
                    </div> -->
                    <!-- menu-box End -->
                </div>
                <!--Accordion End -->
            </div>
  </aside>