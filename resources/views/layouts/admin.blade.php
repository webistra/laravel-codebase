<!DOCTYPE html>
<html>
<head>
  @include('_partials.above-navbar-alert')
  <meta name="description" content="Asset is going to be viral in next 1 year.">
    <!-- Twitter meta-->
   <!--  <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya"> -->
    <!-- Open Graph Meta-->
    <meta property="og:type" content="Asset">
    <meta property="og:site_name" content="Asset Admin">
    <meta property="og:title" content="Asset admin panel">
    <meta name="base_url" content="{{ url('/') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('css/admin-css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/fonts/stylesheet.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin-css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin-css/style_s.css')}}">
     <link rel="stylesheet" href="{{asset('css/admin-css/mobile.css')}}">
     <link rel="stylesheet" href="{{asset('css/admin-css/sweetalert2.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  @yield('stylesheet')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  @include('_partials.admin-header')
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    @include('_partials.admin-navbar')
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2017-2018 <a href="https://adminlte.io">Asset Co.</a>.</strong> All rights
    reserved.
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- All js -->
<script src="{{ asset('js/admin-script/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('/js/jquery.validate.min.js') }}" ></script>
<script src="{{ asset('js/admin-script/popper.min.js')}}"></script>
<script src="{{ asset('js/admin-script/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/admin-script/jquery.nicescroll.min.js')}}"></script>
<script src="{{ asset('js/admin-script/custom.js')}}"></script>
<script src="{{ asset('js/admin-script/sweetalert2.min.js')}}"></script>


@yield('scripts')
</body>
</html>
