<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0,width=device-width,user-scalable=no">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Asset - Real Estate</title>
      <link rel="stylesheet" href="{{ asset('css/web-css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/datepicker.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/nouislider.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/slick.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/slick-theme.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/style.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/mobile.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/toastr.min.css') }}">
      <style>
         .error {
          color: #a20707;
          font-size: 12px;
          margin: 0;
         }
      </style>
   </head>
   <body class="logged">
@include('_partials.logged-in-header')
@yield('content')
<!-- Modal -->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog">
   <div class="modal-dialog max-WT-450" role="document">
      <div class="modal-content">
         <div class="modal-body  d-flex align-items-center text-center minHeight300">
            <div class="w-100">
               <span class="logoutIcon"><img src="{{ asset('img/icon-logout.png') }}" class="logOut-img" alt=""/></span>
               <p class="logOutMsg">Are you sure you want to Logout?</p>
               <div class="modalActionBtn">
                  <a href="{{ route('logout') }}" class="btn shadowGreenBtn max-WT-100">Yes</a>
                  <button type="button" class="btn shadowRedBtn max-WT-100" data-dismiss="modal">No</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
   <script src="{{ asset('js/web-scripts/jquery-3.3.1.min.js')}}"></script>
   <script src="{{ asset('js/jquery.validate.min.js') }}" ></script>
   <script src="{{ asset('js/web-scripts/popper.min.js') }}"></script>
   <script src="{{ asset('js/web-scripts/bootstrap.min.js')}}"></script>
   <script src="{{ asset('js/web-scripts/slick.min.js') }}"></script>
   <script src="{{ asset('js/web-scripts/datepicker.js') }}"></script>
   <script src="{{ asset('js/web-scripts/nouislider.js') }}"></script>
   <script src="{{ asset('js/web-scripts/common.js') }}"></script>
   <script src="{{ asset('js/web-scripts/toastr.min.js') }}"></script>
   <script type="text/javascript">
     $(".alert-success, .alert-info").fadeOut(3000);
   </script>
   @yield('scripts')
</body>
</html>      