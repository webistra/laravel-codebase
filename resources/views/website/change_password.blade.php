@extends('layouts.logged_in')
@section('content')
<main class="main-container">
 <div class="column-tow">
    <div class="container">
       <!-- globle_inside Start -->
       <div class="globle_inside">
          <!-- OuterBlockBox-->
          <!-- outerBlockBox Start -->
          <div class="outerBlockBox">
            @include('_partials.logged-in-sidebar')
                <!-- middleFeedBox Start -->
                <div class="middleFeedBox">
                <!-- globalCard Start -->
                <div class="globalCard">
                  <form id="change-password" class="commonForm mt15" action="{{ route('update-password') }}" method="post">
                    @csrf
                <!-- formBox Start -->
                <div class="formBox center-box max-WT-600">
                <!-- Row Start -->
                <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                      <label class="control-label">Current Password</label>
                      <input type="password" name="old_password" class="form-control custom-control"/>
                   </div>
                   <div class="form-group">
                      <label class="control-label">New Password</label>
                      <input id="password" type="password" name="password" class="form-control custom-control"/>
                   </div>
                   <div class="form-group mb30">
                      <label class="control-label">Confirm Password</label>
                      <input type="password" name="password_confirmation" class="form-control custom-control"/>
                   </div>
                   
                </div>
                </div>
                <!-- Row End -->
                <div class="form-group text-center">
                <button type="submit" class="btn btnYellow  max-WT-200"><span>Update</span></button>
                </div>
                </div>
                <!-- formBox End -->

                </form>
                </div>
                <!-- globalCard End -->            
                </div>
                <!-- middleFeedBox End -->
             @include('_partials.logged-in-footer')
          </div>
          <!-- OuterBlockBox End -->
       </div>
       <!-- globle_inside End -->
    </div>
 </div>
</main>
<!-- Main End -->
@endsection

@section('scripts')
{!! Toastr::message() !!}
<script type="text/javascript">
    $('#change-password').validate({
    focusInvalid: false,
    ignore: [],
    rules: {
         old_password:{
            required:true,
            minlength:8,
            maxlength:16,
            remote: {
            url : "{{ route('validate-password')}}",
            type : "GET",
            dataFilter: function (data) {
                    var json = JSON.parse(data);
                    console.log(json)
                    if (json)
                        return 'true';
                    else 
                        return 'false';
                },    
            },
        },
      password: {
            required: true,
            minlength:8,
            maxlength:16
        },
        password_confirmation: {
            required:true,
            equalTo: "#password"
        },
    },


    messages: {
  
       password: {
        required: 'Please enter new password.',
        
      },
       password_confirmation: {
        required: 'Please enter confirm password.',
        
      },
      old_password:{
                required: 'Please enter old password.',
                remote:  'Old password does not match.'
        }
    },

    submitHandler: function(form) {
      form.submit();
    }
  });
</script>
@endsection