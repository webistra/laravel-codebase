@extends('layouts.app')
@section('content')
<section class="common-section-main">
    <div class="common-container">
        <div class="row">
            <div class="col-12">
                <div class="common-content-body">
                    <div class="form-main-heading">
                        <h3>CONFIRM YOUR ADDRESS</h3>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        </div>
                    @endif
                    <div class="form-body">
                        <form method="post" id="confirm_address" action="{{ route('address.confirm') }}">
                            @csrf   
                            <p class="form-para">Welcome! Please confirm your address to ensure privacy, address verification  is required.
                            </p>
                            <div class="row">
                              <div class="col-sm-12">
                                <div class=" form-group">
                                  <input type="text" name="street_address" class="form-control custom-control" placeholder="Street Address*" value="" required="" minlength="2" maxlength="100"> 
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class=" form-group">
                                   <input type="text" name="apartment" class="form-control custom-control" placeholder="Apartment*" value="" required="" minlength="2" maxlength="100"> 
                                </div>
                              </div>
                              <div class="col-sm-12">
                                    <div class=" form-group">
                                       <input type="email" name="eml" class="form-control custom-control" placeholder="Email*" value="{{ auth()->user()->email }}" readonly="">
                                    </div>
                              </div>
                              <div class="col-sm-12">
                                <div class=" form-group">
                                  <input type="text" name="zipCode" class="form-control custom-control" placeholder="Zip Code*" value="" required> 
                                </div>
                              </div>
                              
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                  <button class="btn btnYellow w-100 minHeight40 font16" type="submit"><span>Confirm Your Address</span></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    $('#confirm_address').validate({
      focusInvalid: false,
      ignore: [],
      rules: {
    	  email: {
    		required: true,
    		 remote: {
    		  url : "{{ route('valid-email')}}",
    		  type : "GET",
    		  dataFilter: function (data) {
    				  var json = JSON.parse(data);
    				  if (json)
    					  return 'false';
    				  else 
    					  return 'true';
    			  },    
    		  },
    	  },
		},
      messages: {
    	email: {
    	  required: 'Please enter email address.',
    	  remote:  'Email address does not exists.'
    	},
        street_address: {
          required: 'Please enter street address.',
        },
        apartment: {
          required:  'Please enter your apartment.'
        },
        zipCode: {
          required:  'Please enter zip code.'
        },
      },
      submitHandler: function(form) {
    	form.submit();
      }
    });
</script>
@endsection