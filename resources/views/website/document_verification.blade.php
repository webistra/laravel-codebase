@extends('layouts.app')
@section('content')
<section class="common-section-main">
    <div class="common-container">
      <div class="row">
         <div class="col-12">
            <div class="common-content-body">
              <div class="form-main-heading">
                <h3>DOCUMENT VERIFICATION</h3>
                  @if (session('status'))
                  <div class="alert alert-success  alert-dismissible">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {{ session('status') }}
                  </div>
                  @endif
                    @if ($errors->any())
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          </div>
              @endif
              </div>
              <div class="form-body">
                <form id="document-verification" action="{{ route('verification.submit') }}" enctype="multipart/form-data" method="post">
                  @csrf
                  <div class="otp-block">
                     <div class="row">
                        <div class="col-sm-12">
                          <div class=" form-group">
                           <p class="form-para">Please upload the document for the verification.</p>
                          </div>
                      </div>
                      <div class="col-sm-12">
                      <div class=" form-group file-Input">
                        <label>Choose File</label>
                        <input type="file" name="docVerify" class="form-control custom-control" placeholder="Choose File image or pdf">
                      </div>
                    </div>
                      <div class="col-sm-12 text-center">
                         <button type="submit" class="btn btnYellow max-WT-200 minHeight40 font16"><span>Submit</span></button>
                      </div>
                     </div>
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>
    </div>
</section>
@endsection