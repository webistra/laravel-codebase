@extends('layouts.logged_in')
@section('content')
<main class="main-container">
 <div class="column-tow">
    <div class="container">
       <!-- globle_inside Start -->
       <div class="globle_inside">
          <!-- OuterBlockBox-->
          <!-- outerBlockBox Start -->
          <div class="outerBlockBox">
            @include('_partials.logged-in-sidebar')
             <!-- middleFeedBox Start -->
             <div class="middleFeedBox">
                <!-- globalCard Start -->
                <div class="globalCard">
                   <!-- formBox Start -->
                   <div class="formBox formBoxHorizontal center-box max-WT-600">
                      <a href="{{ route('edit-profile') }}" class="btn btnYellow max-WT-100 editBtn"><span>Edit Profile</span></a>
                      <div class="profileImg center-box mb15 mt15">
                         @if(!empty(auth()->user()->profile_image))
                          <img id="img_upload" src="{{ url('storage/'.auth()->user()->profile_image)  }}" class="user-image" width="130" height="130">
                        @else
                          <img class="radius100" src="{{ asset('img/no-image.png') }}" alt="Image"/>
                        @endif  
                      </div>
                      <!-- Row Start -->
                      <div class="row">
                         <div class="col-md-12">
                            <div class="form-group d-flex">
                               <label class="control-label">Name</label>
                               <div class="viewField">{{  auth()->user()->name }}</div>
                            </div>
                            <div class="form-group d-flex">
                               <label class="control-label">Email</label>
                               <div class="viewField">{{  auth()->user()->email }}</div>
                            </div>
                            <div class="form-group d-flex">
                               <label class="control-label">Phone Number</label>
                               <div class="viewField">{{  auth()->user()->country_code.auth()->user()->phone_number }}</div>
                            </div>
                            <div class="form-group d-flex">
                               <label class="control-label">Gender</label>
                               <div class="viewField">{{  auth()->user()->gender }}</div>
                            </div>
                            <div class="form-group d-flex">
                               <label class="control-label">DOB</label>
                               <div class="viewField">{{  auth()->user()->date_of_birth }}</div>
                            </div>
                            <div class="form-group d-flex">
                               <label class="control-label">Street Address</label>
                               <div class="viewField">{{  auth()->user()->street_address }}</div>
                            </div>
                            <div class="form-group d-flex mb30">
                               <label class="control-label">Apartment</label>
                               <div class="viewField">{{  auth()->user()->apartment }}</div>
                            </div>
                         </div>
                      </div>
                      <!-- Row End -->
                   </div>
                   <!-- formBox End -->
                   <!-- Row Start -->
                   <div class="row mb15">
                      <div class="col-md-6">
                         <div class="propertyBox">
                            <h6>My Property</h6>
                            <div class="d-flex minHeight140 align-items-center">
                               <div class="w-100">
                                  <a href="my-property.html" class="btn btnYellow minHeight40 lineHeight28 font14 d-block mb10"><span>For Rent</span></a>
                                  <a href="my-property.html" class="btn btnGreen minHeight40 lineHeight28 font14 d-block mb10"><span>For Sell</span></a>
                                  <a href="add-new-property.html" class="btn btnYellow minHeight40 lineHeight28 font14 d-block mb10"><span>Add New</span></a>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-6">
                         <div class="propertyBox">
                            <h6>Purchase Property</h6>
                            <div class="d-flex minHeight140 align-items-center">
                               <div class="w-100">
                                  <a href="bank.html" class="btn btnYellow minHeight40 font14 lineHeight28 d-block mb10"><span>Transactions</span></a>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- Row End -->
				   <h4 class="head4">Community Number</h4>
				   <div class="profileComunityInfo d-flex justify-content-between">
				     <div class="ComunityInfoInner blkColor"><span>35</span>Friends</div>
					 <div class="ComunityInfoInner gColor"><span>{{ $property_following_count }}</span>Property Following</div>
					 <div class="ComunityInfoInner rColor"><span>6</span>Neighbourhood</div>
				   </div>
                </div>
                <!-- globalCard End -->					   
             </div>
             <!-- middleFeedBox End -->
             @include('_partials.logged-in-footer')
          </div>
          <!-- OuterBlockBox End -->
       </div>
       <!-- globle_inside End -->
    </div>
 </div>
</main>
<!-- Main End -->
@endsection

@section('scripts')
{!! Toastr::message() !!}
@endsection