@extends('layouts.app')
@section('content')
<section class="common-section-main">
    <div class="common-container">
      <div class="row">
         <div class="col-12">
            <div class="common-content-body">
              @if (session('status'))
              <div class="alert {{ session('alert-class') }}  alert-dismissible">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session('status') }}
              </div>
              @endif
              <div class="form-main-heading">
                <h3>LOGIN</h3>
              </div>
              <div class="form-body">
                <form id="login" action="{{ route('login') }}" method="post">
                @csrf
                  <p class="form-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                  <div class="row">
                    <div class="col-12 col-sm-6">
                      <div class=" form-group">
                        <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} custom-control input-icon-email" placeholder="Email Address*" value="{{ old('email') }}" required autofocus maxlength="60">
                        @if ($errors->has('email'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                      </div>
                      <div class="form-group">
                        <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} custom-control input-icon-pass" placeholder="Password*" required maxlength="16">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group remember-forgot">
                          <p class="custom-checkbox">
                            <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">Remember me</label>
                          </p>
                          <a href="{{ route('forgot-password') }}" class="forgot-password">Forgot Password? </a>
                      </div>
                      <button type="submit" class="btn btnYellow w-100 minHeight40 font16"><span>Sign In</span></button>
                    </div>
                    <div class="col-12 col-sm-1">
                      <div class="or-text"><span>OR</span> </div>
                    </div>
                    <div class="col-12 col-sm-5">
                      <div class="social-button">
                          <a href="{{route('social.login',['provider' => 'facebook'])}}" class="fb"><span class=""><img src="{{asset('img/facebook.png')}}"></span>Sigin with Facebook</a>
                          <!-- <a href="#" class="twitter"><span class=""><img src="{{asset('img/twitter.png')}}"></span>Sigin with Twitter</a> -->
                          <a href="{{route('social.login',['provider' => 'google'])}}" class="google"><span class=""><img src="{{asset('img/google.png')}}"></span>Sigin with Google +</a>
                      </div>
                    </div>

                  </div>

                   <p class="new-user"> New User? <a href="{{route('register')}}">Signup</a>

                </form>
              </div>
            </div>
        </div>
      </div>
    </div>
</section>


 <!-- forgot Password Modal -->
<div class="modal fade common-modal" id="forgot-pass" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">FORGOT PASSWORD</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body change-pass-body">
        <p class="modal-para">Please enter the registered email address so that we can send you a password reset instructions.</p>

        <div class=" form-group">
          <input type="text" name="" class="form-control custom-control input-icon-email" placeholder="Email Address*">
        </div>
        <div class="mt40">
          <button class="btn btn-blue w-100 btn-common">Submit</button>
        </div>
        <p class="new-user resend">
          <a href="#">Resend Password?</a>
        </p>
      </div>
     
    </div>
    
  </div>
</div>
<!--      -->

@endsection

@section('scripts')
<script type="text/javascript">
  $('#login').validate({
    focusInvalid: false,
    ignore: [],
    rules: {
        email: {
          required: true,
           remote: {
            url : "{{ route('valid-email')}}",
            type : "GET",
            dataFilter: function (data) {
                    var json = JSON.parse(data);
                    console.log(json)
                    if (json)
                        return 'false';
                    else 
                        return 'true';
                },    
            },
        },
      password: {
        required: true
      }
    },
    messages: {
      email: {
        required: 'Please enter email address.',
        remote:  'Email address does not exists.'
      },
      password: {
        required: 'Please enter password.',
        
      }
    },
    submitHandler: function(form) {
      form.submit();
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") == "email" || element.attr("name") == "password" )
            $('.invalid-feedback').remove();
            error.insertAfter(element);
    }
  });
</script>
@endsection