<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'properties',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
                $table->enum('property_for', [1, 2])->comment('1 for sell, 2 for rent');
                $table->string('property_name')->nullable();
                $table->string('property_address')->nullable();
                $table->string('zip_code')->nullable();
                $table->string('latitude')->nullable();
                $table->string('longitude')->nullable();
                $table->enum('property_type', [1, 2, 3])->comment('1 for apartment, 2 for house, 3 for room');
                $table->integer('no_of_bedrooms')->nullable();
                $table->integer('no_of_bathrooms')->nullable();
                $table->integer('other_living_room')->nullable();
                $table->integer('parking_garage')->nullable();
                $table->longText('preffered_suburb')->nullable();
                $table->longText('reason_for_selling')->nullable();
                $table->longText('description')->nullable();
                $table->double('price_for_property')->nullable();
                $table->string('price_type')->nullable()->comment('monthly or weekly');
                $table->string('main_image')->nullable()->comment('Property main image');
                $table->enum('is_pet_allowed',[1, 2])->comment('0 for not allow, 1 for allowed');
                $table->boolean('is_deleted')->default(false)->comment('0 For not deleted, 1 for deleted');
                $table->boolean('is_blocked')->default(false)->comment('0 For not blocked, 1 for blocked');
                $table->boolean('property_status')->default(false)->comment('0 for active, 1 for properties sold or rent');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
