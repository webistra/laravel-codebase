function headerbg() {
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $("header").addClass("header-bg");
    } else {
        $("header").removeClass("header-bg");
    }
}

$(window).scroll(function() {
    headerbg();
});

$(function() {

    // Menu Script
    $('.btn-mobmenu').on('click', function() {
        $('.menu,.leftMenuBox').toggleClass('mobmenushow');
        $(this).children('.fa').toggleClass('fa-bars fa-times');
    });
    $('.plus-minus').on('click', function() {
        $(this).next().slideToggle();
        $(this).toggleClass('fa-minus fa-plus');
    });
    //End Menu Script

    // $('#otpModal').modal('show');
});

$(function() {

    // Menu Script
    $('.cross-icon').on('click', function() {
        $('.right-sidebar_s').toggleClass('right-sidebar-hide');
        $(this).children('.fa').toggleClass('fa-bars fa-times');
    });
    $('.plus-minus').on('click', function() {
        $(this).next().slideToggle();
        $(this).toggleClass('fa-minus fa-plus');
    });
    //End Menu Script

    // $('#otpModal').modal('show');
});

$('.panel-collapse').on('show.bs.collapse', function() {
    $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function() {
    $(this).siblings('.panel-heading').removeClass('active');
});

$(".modeDropdown").click(function(e) {
    e.stopPropagation();
});



/*********setHeight************/

var windowWidth = $(window).width();
if (windowWidth >= 768) {
    var headHeight = $('.headHeight').innerHeight();
    var getHeight = $('.getHeight').outerHeight();
    $('.setHeightMap').css('height', getHeight - headHeight);
};

$(document).ready(function() {
	/* =============Datepicker=========== */
	$('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy',
	  autoclose: true
    });
	/* ===================Price Range Slider=============== */
		
		if ($('#property-price-range')[0]) {
			var propertyPriceRange = document.getElementById('property-price-range');
			var propertyPriceRangeValues = [
				document.getElementById('property-price-upper'),
				document.getElementById('property-price-lower')
			]

			noUiSlider.create (propertyPriceRange, {
				start: [0, 1000],
				connect: true,
				range: {
					'min': 0,
					'max': 1000
				}
			});

			propertyPriceRange.noUiSlider.on('update', function( values, handle ) {
				propertyPriceRangeValues[handle].innerHTML = values[handle];
			});
		}
	
	
	if ($('#radius-range')[0]) {
			var radiusPriceRange = document.getElementById('radius-range');
			var radiusPriceRangeValues = [
				document.getElementById('radius-range-upper'),
				document.getElementById('radius-range-lower')
			]

			noUiSlider.create (radiusPriceRange, {
				start: [0, 1000],
				connect: true,
				range: {
					'min': 0,
					'max': 1000
				}
			});

			radiusPriceRange.noUiSlider.on('update', function( values, handle ) {
				radiusPriceRangeValues[handle].innerHTML = values[handle];
			});
		}
		
		
		if ($('#lowest-highest-range')[0]) {
			var lowestHighestPriceRange = document.getElementById('lowest-highest-range');
			var lowestHighestRangeValues = [
				document.getElementById('lowest-price'),
				document.getElementById('highest-price')
			]

			noUiSlider.create (lowestHighestPriceRange, {
				start: [0, 1000],
				connect: true,
				range: {
					'min': 0,
					'max': 1000
				}
			});

			lowestHighestPriceRange.noUiSlider.on('update', function( values, handle ) {
				lowestHighestRangeValues[handle].innerHTML = values[handle];
			});
		}
	
	
/* ============Price Range Slider End========== */
	
    $(document).on('click', '.browse', function() {
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });
    $(document).on('change', '.file', function() {
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    /* ========For Image Change========== */
    $('.profileImg input[type=file]').change(function() {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.profileImg img').attr('src', e.target.result).fadeIn('slow');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    /* =========Image Changes====== */



    /* Toggle Header User info Drop Down */
    $(document).on("click", ".user_top_box ", function() {
        $(".user_top_box .dropdown-menu").toggleClass("show");
    });

    $(document).click(function(e) {
        if (!$(e.target).is(".head-drop-down, .head-drop-down *, .user_top_box, .user_top_box *")) {
            $(".user_top_box .dropdown-menu").removeClass('show');
        }
    });

    $(".commentTap").click(function() {
        $(this).parents(".feedActionList").siblings("div.feedCommentBox").addClass("show");
    });

    $(".openFilter").click(function() {
        $(".filterFieldBox").toggleClass("hide");
        $(".sortFieldBox").addClass("hide");
    });
    $(".opensortFilter").click(function() {
        $(".sortFieldBox").toggleClass("hide");
        $(".filterFieldBox").addClass("hide");
    });

    $('.feedSlider').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 1000,
    });


    $('.carousel').carousel({
        interval: 3000
    })

    /* =================Upload File End======== */
    if ($(window).width() > 1024) {
        /*====For Left Menu Box====*/
        var middleBoxoffsetLeft = $(".middleFeedBox").offset().left;
        var leftMenuWidth = $(".leftMenuBox").width();
        middleBoxoffsetLeft = middleBoxoffsetLeft - leftMenuWidth;
        
		/*====For Right Content Box====*/
        var rightContentWidth = $(".rightContentBox").width();
        var rightContentOffsetLeft = $(".rightContentBox").offset().left;
        rightContentOffsetLeft = rightContentOffsetLeft - rightContentWidth + 30;
        setTimeout(function() {
            $(".leftMenuBox,.rightContentBox").addClass("animate");
            $(".leftMenuBox").css({
                "left": middleBoxoffsetLeft
            });
            $(".rightContentBox").css({
                "right": rightContentOffsetLeft
            });

        }, 100);

    } /* ==========If End========= */
   
   /* =====Open Comment Box======== */
    $(".commentField input[type=text]").keypress(function(e) {
    if(e.which == 13) {
       if($(this).val()==""){
		  
	   }
	   else{
		   var thisVal=$(this).val();
		   $(".commentBoxOuter").prepend("<div class='postCommentBox hide'><span class=feedUserImg><img src=images/write-img.png alt=image></span><div class=postCommentDetail><p><a class=postUserName href=profile.html>David</a>"+thisVal+"</p><div class=reComment><button class=reLikeBtn>Like</button><button class=reReplyBtn>Reply</button><span class=reCommtTime>Mar 6 at 11:00 am</span></div></div></div>");
		   $(this).parents("div.feedCommentBox").siblings("div.commentBoxOuter").find("div.postCommentBox").removeClass("hide"); 

           	   
	   }
     }
    });
   
   /* ===========Compose Comment Style=========== */
   $(".composeCommentNotExpand").click(function(){
	  $("body").addClass("expandCompose");
	  $(".composeCommentBox").removeClass("composeCommentNotExpand");
   });
   
    $(".closeCompose").click(function(){
		setTimeout(function(){
		  $("body").removeClass("expandCompose");
		  $(".composeCommentBox").addClass("composeCommentNotExpand");
	  },100);
   });
   
   $(document).click(function(e) {
        if (!$(e.target).is(".closeCompose, .closeCompos *, .composeCommentBox, .composeCommentBox *")) {
            $("body").removeClass("expandCompose");
		    $(".composeCommentBox").addClass("composeCommentNotExpand");
        }
    });
   
   
   /* =============AutoExpand textarea=========== */
     var textarea = document.querySelector('.composeAreaField textarea');

		textarea.addEventListener('keydown', autosize);
					 
		function autosize(){
		  var el = this;
		  setTimeout(function(){
			el.style.cssText = 'height:auto; padding:0';
			// for box-sizing other than "content-box" use:
			// el.style.cssText = '-moz-box-sizing:content-box';
			el.style.cssText = 'height:' + el.scrollHeight + 'px';
		  },0);
		}
		
	
    	
   
   
   

    if ($(window).width() > 767) {
        $(".chatScroll,.chatNavList").niceScroll({
            cursorborder: "",
            cursorcolor: "#ccc",
            boxzoom: false
        });
    }
	
	

}); /*======== Ready End======= */